unit data;

interface

uses
  SysUtils, Classes, DB, ADODB, Dialogs, DateUtils;

type
  Tdm = class(TDataModule)
    db: TADOConnection;
    dscConfig: TDataSource;
    tblConfig: TADOTable;
    dscNews: TDataSource;
    tblNews: TADOTable;
    dscQF: TDataSource;
    tblQF: TADOTable;
    tblNewsClientID: TIntegerField;
    tblNewsTerminalID: TIntegerField;
    tblNewsHeadLine: TWideStringField;
    tblNewsDesc: TWideStringField;
    tblNewsStartDate: TDateTimeField;
    tblNewsStopDate: TDateTimeField;
    tblNewsActive: TBooleanField;
    tblNewsCUser: TWideStringField;
    tblNewsCDate: TDateTimeField;
    tblQFClientID: TIntegerField;
    tblQFTerminalID: TIntegerField;
    tblQFFileName: TWideStringField;
    tblQFFileType: TWideStringField;
    tblQFFileSize: TIntegerField;
    tblQFFileLocation: TWideStringField;
    tblQFActive: TBooleanField;
    tblQFCUser: TWideStringField;
    tblQFCDate: TDateTimeField;
    tblConfigID: TAutoIncField;
    tblConfigClientID: TIntegerField;
    tblConfigTerminalID: TIntegerField;
    tblConfigshow_news: TBooleanField;
    tblConfigshow_ads: TBooleanField;
    tblConfigDefault_video: TWideStringField;
    tblConfigvideo_directory: TWideStringField;
    tblConfigvideo_program: TWideStringField;
    tblConfignews_program: TWideStringField;
    tblConfighttp_server: TWideStringField;
    tblConfigvideo_source: TBooleanField;
    tblQFQID: TIntegerField;
    tblConfigrefresh_time: TIntegerField;
    tblConfigftp_server: TWideStringField;
    tblConfigftp_port: TIntegerField;
    tblConfigftp_user: TWideStringField;
    tblConfigftp_pass: TWideStringField;
    tblQFOrdering: TIntegerField;
    tblQFAvail: TBooleanField;
    tblNewsID: TIntegerField;
    tblQFID: TIntegerField;
    tblConfigftp_path: TWideStringField;
    tblConfigftp_passive: TBooleanField;
    dscPlayQ: TDataSource;
    tblPlayQ: TADOTable;
    tblPlayQID: TIntegerField;
    tblConfigdebug_mode: TBooleanField;
    tblQFNewFileName: TWideStringField;
    tblConfigsys_pwd: TWideStringField;
    dscQ: TDataSource;
    tblQ: TADOTable;
    tblQID: TIntegerField;
    tblQQueue_no: TWideStringField;
    tblQClientID: TIntegerField;
    tblQTerminalID: TIntegerField;
    tblQQName: TWideStringField;
    tblQDesc: TWideStringField;
    tblQStartDate: TDateTimeField;
    tblQStopDate: TDateTimeField;
    tblQActive: TBooleanField;
    tblQCUser: TWideStringField;
    tblQCDate: TDateTimeField;
    tblQRunning: TBooleanField;
    tblConfignews_font: TWideStringField;
    tblConfignews_size: TIntegerField;
    tblConfignews_color: TIntegerField;
    tblConfignews_isbold: TIntegerField;
    tblConfignews_isitalic: TIntegerField;
    tblConfignews_isunderline: TIntegerField;
    tblConfigbanner_mode: TIntegerField;
    tblConfigbanner_show_interval: TIntegerField;
    dscBanner: TDataSource;
    tblBanner: TADOTable;
    tblBannerID: TIntegerField;
    tblBannerClientID: TIntegerField;
    tblBannerTerminalID: TIntegerField;
    tblBannerFileName: TWideStringField;
    tblBannerNewFileName: TWideStringField;
    tblBannerShowType: TIntegerField;
    tblBannerShowPosition: TIntegerField;
    tblBannerFileSize: TIntegerField;
    tblBannerFileLocation: TWideStringField;
    tblBannerStartDate: TDateTimeField;
    tblBannerStopDate: TDateTimeField;
    tblBannerActive: TBooleanField;
    tblBannerCUser: TWideStringField;
    tblBannerCDate: TDateTimeField;
    dscPlayBq1: TDataSource;
    tblPlayBq1: TADOTable;
    IntegerField1: TIntegerField;
    dscPlayBq2: TDataSource;
    tblPlayBq2: TADOTable;
    IntegerField2: TIntegerField;
    tblConfignews_delay: TIntegerField;
    tblConfignews_height: TIntegerField;
    tblConfigbanner_width: TIntegerField;
    tblConfignews_back_color: TIntegerField;
    tblConfignews_step: TIntegerField;
    tblConfigbanner_prop: TBooleanField;
    tblConfigshow_clock: TBooleanField;
    tblConfigrss_active: TBooleanField;
    tblConfigrss_url: TWideStringField;
    tblQIsMonday: TBooleanField;
    tblQIsTuesday: TBooleanField;
    tblQIsWednesday: TBooleanField;
    tblQIsThursday: TBooleanField;
    tblQIsFriday: TBooleanField;
    tblQIsSaturday: TBooleanField;
    tblQIsSunday: TBooleanField;
    tblQStartInterval: TDateTimeField;
    tblQEndInterval: TDateTimeField;
    tblConfiglogo_width: TIntegerField;
    tblConfiglogo_ratio: TBooleanField;
    tblConfiglogo_stretch: TBooleanField;
    tblConfigdef_type: TIntegerField;
    tblConfigdef_file: TWideStringField;
    tblFileList: TADOTable;
    tblBannerQID: TIntegerField;
    tblBannerFileHash: TWideStringField;
    tblPlayQQID: TIntegerField;
    tblQFFileHash: TWideStringField;
    tblQOrder: TIntegerField;
    tblNewsQID: TIntegerField;
    tblActiveQ: TADOTable;
    tblPlayBanner1: TADOTable;
    tblPlayBanner2: TADOTable;
    tblPlayVideo: TADOTable;
    tblPlayNews: TADOTable;
    tblPlayLayout: TADOTable;
    tblQueuePlay: TADOTable;
    tblPlayBanner1QID: TIntegerField;
    tblPlayBanner1id: TIntegerField;
    tblPlayBanner1FileName: TWideStringField;
    tblPlayBanner1NewFileName: TWideStringField;
    tblPlayBanner1ShowType: TIntegerField;
    tblPlayBanner1ShowPosition: TIntegerField;
    tblPlayBanner1FileSize: TIntegerField;
    tblPlayBanner1FileLocation: TWideStringField;
    tblPlayBanner1FileHash: TWideStringField;
    tblPlayBanner2QID: TIntegerField;
    tblPlayBanner2id: TIntegerField;
    tblPlayBanner2FileName: TWideStringField;
    tblPlayBanner2NewFileName: TWideStringField;
    tblPlayBanner2ShowType: TIntegerField;
    tblPlayBanner2ShowPosition: TIntegerField;
    tblPlayBanner2FileSize: TIntegerField;
    tblPlayBanner2FileLocation: TWideStringField;
    tblPlayBanner2FileHash: TWideStringField;
    tblPlayVideoID: TIntegerField;
    tblPlayVideoQID: TIntegerField;
    tblPlayVideoClientID: TIntegerField;
    tblPlayVideoTerminalID: TIntegerField;
    tblPlayVideoFileName: TWideStringField;
    tblPlayVideoNewFileName: TWideStringField;
    tblPlayVideoFileType: TWideStringField;
    tblPlayVideoFileSize: TIntegerField;
    tblPlayVideoFileLocation: TWideStringField;
    tblPlayVideoFileHash: TWideStringField;
    tblPlayVideoActive: TBooleanField;
    tblPlayVideoAvail: TBooleanField;
    tblPlayVideoOrdering: TIntegerField;
    tblPlayNewsQID: TIntegerField;
    tblPlayNewsid: TIntegerField;
    tblPlayNewsHeadLine: TWideStringField;
    tblQueuePlayid: TIntegerField;
    tblQueuePlayQName: TWideStringField;
    tblQueuePlayStartDate: TDateTimeField;
    tblQueuePlayStopDate: TDateTimeField;
    tblQueuePlaySTime: TDateTimeField;
    tblQueuePlayFTime: TDateTimeField;
    tblQueuePlayCTime: TDateTimeField;
    tblActiveQID: TIntegerField;
    tblLogPlay: TADOTable;
    tblLogPlayID: TAutoIncField;
    tblLogPlayqid: TIntegerField;
    tblLogPlaylog_id: TIntegerField;
    tblLogPlaytype: TWideStringField;
    tblLogPlayfile: TWideStringField;
    tblLogPlayaction: TWideStringField;
    tblLogPlaycdate: TWideStringField;
    tblPlayBanner1Ordering: TIntegerField;
    tblPlayBanner2Ordering: TIntegerField;
    tblPlayLayoutID: TAutoIncField;
    tblPlayLayoutClientID: TIntegerField;
    tblPlayLayoutTerminalID: TIntegerField;
    tblPlayLayoutQID: TIntegerField;
    tblPlayLayoutshow_news: TBooleanField;
    tblPlayLayoutshow_ads: TBooleanField;
    tblPlayLayoutDefault_video: TWideStringField;
    tblPlayLayoutvideo_directory: TWideStringField;
    tblPlayLayouthttp_server: TWideStringField;
    tblPlayLayoutvideo_program: TWideStringField;
    tblPlayLayoutnews_program: TWideStringField;
    tblPlayLayoutrefresh_time: TIntegerField;
    tblPlayLayoutftp_server: TWideStringField;
    tblPlayLayoutftp_port: TIntegerField;
    tblPlayLayoutftp_user: TWideStringField;
    tblPlayLayoutftp_pass: TWideStringField;
    tblPlayLayoutftp_path: TWideStringField;
    tblPlayLayoutftp_passive: TBooleanField;
    tblPlayLayoutdebug_mode: TBooleanField;
    tblPlayLayoutsys_pwd: TWideStringField;
    tblPlayLayoutnews_font: TWideStringField;
    tblPlayLayoutnews_size: TIntegerField;
    tblPlayLayoutnews_color: TIntegerField;
    tblPlayLayoutnews_isbold: TIntegerField;
    tblPlayLayoutnews_isitalic: TIntegerField;
    tblPlayLayoutnews_isunderline: TIntegerField;
    tblPlayLayoutbanner_mode: TIntegerField;
    tblPlayLayoutbanner_show_interval: TIntegerField;
    tblPlayLayoutnews_delay: TIntegerField;
    tblPlayLayoutnews_height: TIntegerField;
    tblPlayLayoutbanner_width: TIntegerField;
    tblPlayLayoutnews_back_color: TIntegerField;
    tblPlayLayoutnews_step: TIntegerField;
    tblPlayLayoutbanner_prop: TBooleanField;
    tblPlayLayoutshow_clock: TBooleanField;
    tblPlayLayoutrss_active: TBooleanField;
    tblPlayLayoutrss_url: TWideStringField;
    tblPlayLayoutvideo_source: TBooleanField;
    tblPlayLayoutlogo_width: TIntegerField;
    tblPlayLayoutlogo_stretch: TBooleanField;
    tblPlayLayoutlogo_ratio: TBooleanField;
    tblPlayLayoutdef_type: TIntegerField;
    tblPlayLayoutdef_file: TWideStringField;
    tblConfigQID: TIntegerField;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure FindPlayQIdx(idx: integer);
    procedure FindPlayBQ1Idx(idx: integer);
    procedure FindPlayBQ2Idx(idx: integer);
  end;

var
  dm: Tdm;

implementation

{$R *.dfm}

procedure Tdm.FindPlayQIdx(idx: integer);
var
  i: integer;
begin     // start the first record call idx = 0
  with tblPlayQ do
  begin
    first;

    for i := 1 to idx do
    begin
      next;
    end;

  end;
end;

procedure Tdm.FindPlayBQ1Idx(idx: integer);
var
  i: integer;
begin     // start the first record call idx = 0
  with tblPlayBq1 do
  begin
    first;

    for i := 1 to idx do
    begin
      next;
    end;

  end;
end;

procedure Tdm.FindPlayBQ2Idx(idx: integer);
var
  i: integer;
begin     // start the first record call idx = 0
  with tblPlayBq2 do
  begin
    first;

    for i := 1 to idx do
    begin
      next;
    end;

  end;
end;

end.
