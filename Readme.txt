﻿Version 1.3.1
 - ใช้ Chromium เป็น Component ในการแสดง HTML
 - ปรับแก้ไขรายละเอียดจาก url.ini
 - รองรับ  HTML
 - ปรับตำแหน่ง HTML ได้ (LEFT,RIGHT,TOP,BOTTOM)
 - ตั้ง URL ได้เองและแสดงวนเมื่อครบ
 - ตั้งเวลาที่ URL จะแสดงผลแสดงแต่ละ URL นาน X วินาที