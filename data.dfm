object dm: Tdm
  OldCreateOrder = False
  Left = 342
  Top = 703
  Height = 772
  Width = 487
  object db: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Password="";User ID=Admin;Data ' +
      'Source=db.mdb;Mode=Share Deny None;Extended Properties="";Jet OL' +
      'EDB:System database="";Jet OLEDB:Registry Path="";Jet OLEDB:Data' +
      'base Password=9999;Jet OLEDB:Engine Type=5;Jet OLEDB:Database Lo' +
      'cking Mode=1;Jet OLEDB:Global Partial Bulk Ops=2;Jet OLEDB:Globa' +
      'l Bulk Transactions=1;Jet OLEDB:New Database Password="";Jet OLE' +
      'DB:Create System Database=False;Jet OLEDB:Encrypt Database=False' +
      ';Jet OLEDB:Don'#39't Copy Locale on Compact=False;Jet OLEDB:Compact ' +
      'Without Replica Repair=False;Jet OLEDB:SFP=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 40
    Top = 24
  end
  object dscConfig: TDataSource
    DataSet = tblConfig
    Left = 120
    Top = 80
  end
  object tblConfig: TADOTable
    Connection = db
    CursorType = ctStatic
    TableName = 'configs'
    Left = 40
    Top = 80
    object tblConfigID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object tblConfigClientID: TIntegerField
      FieldName = 'ClientID'
    end
    object tblConfigTerminalID: TIntegerField
      FieldName = 'TerminalID'
    end
    object tblConfigshow_news: TBooleanField
      FieldName = 'show_news'
    end
    object tblConfigshow_ads: TBooleanField
      FieldName = 'show_ads'
    end
    object tblConfigVideo_source: TBooleanField
      FieldName = 'video_source'
    end
    object tblConfigDefault_video: TWideStringField
      FieldName = 'Default_video'
      Size = 100
    end
    object tblConfigvideo_directory: TWideStringField
      FieldName = 'video_directory'
      Size = 255
    end
    object tblConfighttp_server: TWideStringField
      FieldName = 'http_server'
      Size = 100
    end
    object tblConfigvideo_program: TWideStringField
      FieldName = 'video_program'
      Size = 255
    end
    object tblConfignews_program: TWideStringField
      FieldName = 'news_program'
      Size = 255
    end
    object tblConfigrefresh_time: TIntegerField
      FieldName = 'refresh_time'
    end
    object tblConfigftp_server: TWideStringField
      FieldName = 'ftp_server'
      Size = 200
    end
    object tblConfigftp_port: TIntegerField
      FieldName = 'ftp_port'
    end
    object tblConfigftp_user: TWideStringField
      FieldName = 'ftp_user'
      Size = 50
    end
    object tblConfigftp_pass: TWideStringField
      FieldName = 'ftp_pass'
      Size = 50
    end
    object tblConfigftp_path: TWideStringField
      FieldName = 'ftp_path'
      Size = 100
    end
    object tblConfigftp_passive: TBooleanField
      FieldName = 'ftp_passive'
    end
    object tblConfigdebug_mode: TBooleanField
      FieldName = 'debug_mode'
    end
    object tblConfigsys_pwd: TWideStringField
      FieldName = 'sys_pwd'
      Size = 255
    end
    object tblConfignews_font: TWideStringField
      FieldName = 'news_font'
      Size = 255
    end
    object tblConfignews_size: TIntegerField
      FieldName = 'news_size'
    end
    object tblConfignews_color: TIntegerField
      FieldName = 'news_color'
    end
    object tblConfignews_isbold: TIntegerField
      FieldName = 'news_isbold'
    end
    object tblConfignews_isitalic: TIntegerField
      FieldName = 'news_isitalic'
    end
    object tblConfignews_isunderline: TIntegerField
      FieldName = 'news_isunderline'
    end
    object tblConfigbanner_mode: TIntegerField
      FieldName = 'banner_mode'
    end
    object tblConfigbanner_show_interval: TIntegerField
      FieldName = 'banner_show_interval'
    end
    object tblConfignews_delay: TIntegerField
      FieldName = 'news_delay'
    end
    object tblConfignews_height: TIntegerField
      FieldName = 'news_height'
    end
    object tblConfigbanner_width: TIntegerField
      FieldName = 'banner_width'
    end
    object tblConfignews_back_color: TIntegerField
      FieldName = 'news_back_color'
    end
    object tblConfignews_step: TIntegerField
      FieldName = 'news_step'
    end
    object tblConfigbanner_prop: TBooleanField
      FieldName = 'banner_prop'
    end
    object tblConfigshow_clock: TBooleanField
      FieldName = 'show_clock'
    end
    object tblConfigrss_active: TBooleanField
      FieldName = 'rss_active'
    end
    object tblConfigrss_url: TWideStringField
      FieldName = 'rss_url'
      Size = 255
    end
    object tblConfiglogo_width: TIntegerField
      FieldName = 'logo_width'
    end
    object tblConfiglogo_ratio: TBooleanField
      FieldName = 'logo_ratio'
    end
    object tblConfiglogo_stretch: TBooleanField
      FieldName = 'logo_stretch'
    end
    object tblConfigdef_type: TIntegerField
      FieldName = 'def_type'
    end
    object tblConfigdef_file: TWideStringField
      FieldName = 'def_file'
      Size = 128
    end
    object tblConfigQID: TIntegerField
      FieldName = 'QID'
    end
  end
  object dscNews: TDataSource
    DataSet = tblNews
    Left = 120
    Top = 152
  end
  object tblNews: TADOTable
    Connection = db
    CursorType = ctStatic
    IndexFieldNames = 'ID'
    TableName = 'news'
    Left = 40
    Top = 152
    object tblNewsID: TIntegerField
      FieldName = 'ID'
    end
    object tblNewsQID: TIntegerField
      FieldName = 'QID'
    end
    object tblNewsClientID: TIntegerField
      FieldName = 'ClientID'
    end
    object tblNewsTerminalID: TIntegerField
      FieldName = 'TerminalID'
    end
    object tblNewsHeadLine: TWideStringField
      FieldName = 'HeadLine'
      Size = 255
    end
    object tblNewsDesc: TWideStringField
      FieldName = 'Desc'
      Size = 255
    end
    object tblNewsStartDate: TDateTimeField
      FieldName = 'StartDate'
    end
    object tblNewsStopDate: TDateTimeField
      FieldName = 'StopDate'
    end
    object tblNewsActive: TBooleanField
      FieldName = 'Active'
    end
    object tblNewsCUser: TWideStringField
      FieldName = 'CUser'
      Size = 50
    end
    object tblNewsCDate: TDateTimeField
      FieldName = 'CDate'
    end
  end
  object dscQF: TDataSource
    DataSet = tblQF
    Left = 120
    Top = 280
  end
  object tblQF: TADOTable
    Connection = db
    CursorType = ctStatic
    TableName = 'queue_files'
    Left = 40
    Top = 280
    object tblQFID: TIntegerField
      FieldName = 'ID'
    end
    object tblQFQID: TIntegerField
      FieldName = 'QID'
    end
    object tblQFClientID: TIntegerField
      FieldName = 'ClientID'
    end
    object tblQFTerminalID: TIntegerField
      FieldName = 'TerminalID'
    end
    object tblQFFileName: TWideStringField
      FieldName = 'FileName'
      Size = 255
    end
    object tblQFNewFileName: TWideStringField
      FieldName = 'NewFileName'
      Size = 255
    end
    object tblQFFileType: TWideStringField
      FieldName = 'FileType'
      Size = 255
    end
    object tblQFFileSize: TIntegerField
      FieldName = 'FileSize'
    end
    object tblQFFileLocation: TWideStringField
      FieldName = 'FileLocation'
      Size = 255
    end
    object tblQFActive: TBooleanField
      FieldName = 'Active'
    end
    object tblQFAvail: TBooleanField
      FieldName = 'Avail'
    end
    object tblQFOrdering: TIntegerField
      FieldName = 'Ordering'
    end
    object tblQFCUser: TWideStringField
      FieldName = 'CUser'
      Size = 50
    end
    object tblQFCDate: TDateTimeField
      FieldName = 'CDate'
    end
    object tblQFFileHash: TWideStringField
      FieldName = 'FileHash'
      Size = 50
    end
  end
  object dscPlayQ: TDataSource
    DataSet = tblPlayQ
    Left = 120
    Top = 352
  end
  object tblPlayQ: TADOTable
    Connection = db
    CursorType = ctStatic
    TableName = 'playq'
    Left = 40
    Top = 352
    object tblPlayQID: TIntegerField
      FieldName = 'ID'
    end
    object tblPlayQQID: TIntegerField
      FieldName = 'QID'
    end
  end
  object dscQ: TDataSource
    DataSet = tblQ
    Left = 120
    Top = 216
  end
  object tblQ: TADOTable
    Connection = db
    CursorType = ctStatic
    TableName = 'queue'
    Left = 40
    Top = 216
    object tblQID: TIntegerField
      FieldName = 'ID'
    end
    object tblQQueue_no: TWideStringField
      FieldName = 'Queue_no'
      Size = 255
    end
    object tblQClientID: TIntegerField
      FieldName = 'ClientID'
    end
    object tblQTerminalID: TIntegerField
      FieldName = 'TerminalID'
    end
    object tblQQName: TWideStringField
      FieldName = 'QName'
      Size = 255
    end
    object tblQDesc: TWideStringField
      FieldName = 'Desc'
      Size = 255
    end
    object tblQStartDate: TDateTimeField
      FieldName = 'StartDate'
    end
    object tblQStopDate: TDateTimeField
      FieldName = 'StopDate'
    end
    object tblQActive: TBooleanField
      FieldName = 'Active'
    end
    object tblQCUser: TWideStringField
      FieldName = 'CUser'
      Size = 50
    end
    object tblQCDate: TDateTimeField
      FieldName = 'CDate'
    end
    object tblQRunning: TBooleanField
      FieldName = 'Running'
    end
    object tblQIsMonday: TBooleanField
      FieldName = 'IsMonday'
    end
    object tblQIsTuesday: TBooleanField
      FieldName = 'IsTuesday'
    end
    object tblQIsWednesday: TBooleanField
      FieldName = 'IsWednesday'
    end
    object tblQIsThursday: TBooleanField
      FieldName = 'IsThursday'
    end
    object tblQIsFriday: TBooleanField
      FieldName = 'IsFriday'
    end
    object tblQIsSaturday: TBooleanField
      FieldName = 'IsSaturday'
    end
    object tblQIsSunday: TBooleanField
      FieldName = 'IsSunday'
    end
    object tblQStartInterval: TDateTimeField
      FieldName = 'StartInterval'
    end
    object tblQEndInterval: TDateTimeField
      FieldName = 'EndInterval'
    end
    object tblQOrder: TIntegerField
      FieldName = 'Order'
    end
  end
  object dscBanner: TDataSource
    DataSet = tblBanner
    Left = 120
    Top = 432
  end
  object tblBanner: TADOTable
    Connection = db
    CursorType = ctStatic
    IndexFieldNames = 'id'
    TableName = 'banner'
    Left = 40
    Top = 432
    object tblBannerID: TIntegerField
      FieldName = 'ID'
    end
    object tblBannerClientID: TIntegerField
      FieldName = 'ClientID'
    end
    object tblBannerTerminalID: TIntegerField
      FieldName = 'TerminalID'
    end
    object tblBannerFileName: TWideStringField
      FieldName = 'FileName'
      Size = 255
    end
    object tblBannerNewFileName: TWideStringField
      FieldName = 'NewFileName'
      Size = 255
    end
    object tblBannerShowType: TIntegerField
      FieldName = 'ShowType'
    end
    object tblBannerShowPosition: TIntegerField
      FieldName = 'ShowPosition'
    end
    object tblBannerFileSize: TIntegerField
      FieldName = 'FileSize'
    end
    object tblBannerFileLocation: TWideStringField
      FieldName = 'FileLocation'
      Size = 255
    end
    object tblBannerStartDate: TDateTimeField
      FieldName = 'StartDate'
    end
    object tblBannerStopDate: TDateTimeField
      FieldName = 'StopDate'
    end
    object tblBannerActive: TBooleanField
      FieldName = 'Active'
    end
    object tblBannerCUser: TWideStringField
      FieldName = 'CUser'
      Size = 50
    end
    object tblBannerCDate: TDateTimeField
      FieldName = 'CDate'
    end
    object tblBannerQID: TIntegerField
      FieldName = 'QID'
    end
    object tblBannerFileHash: TWideStringField
      FieldName = 'FileHash'
      Size = 50
    end
  end
  object dscPlayBq1: TDataSource
    DataSet = tblPlayBq1
    Left = 296
    Top = 400
  end
  object tblPlayBq1: TADOTable
    Connection = db
    CursorType = ctStatic
    TableName = 'playbq1'
    Left = 216
    Top = 400
    object IntegerField1: TIntegerField
      FieldName = 'ID'
    end
  end
  object dscPlayBq2: TDataSource
    DataSet = tblPlayBq2
    Left = 296
    Top = 464
  end
  object tblPlayBq2: TADOTable
    Connection = db
    CursorType = ctStatic
    TableName = 'playbq2'
    Left = 216
    Top = 464
    object IntegerField2: TIntegerField
      FieldName = 'ID'
    end
  end
  object tblFileList: TADOTable
    Connection = db
    CursorType = ctStatic
    TableName = 'filelist'
    Left = 40
    Top = 504
  end
  object tblActiveQ: TADOTable
    Connection = db
    TableName = 'queue'
    Left = 40
    Top = 568
    object tblActiveQID: TIntegerField
      FieldName = 'ID'
    end
  end
  object tblPlayBanner1: TADOTable
    Connection = db
    CursorType = ctStatic
    IndexFieldNames = 'ordering'
    TableName = 'QryPlayBanner1'
    Left = 384
    Top = 32
    object tblPlayBanner1QID: TIntegerField
      FieldName = 'QID'
    end
    object tblPlayBanner1id: TIntegerField
      FieldName = 'id'
    end
    object tblPlayBanner1FileName: TWideStringField
      FieldName = 'FileName'
      Size = 255
    end
    object tblPlayBanner1NewFileName: TWideStringField
      FieldName = 'NewFileName'
      Size = 255
    end
    object tblPlayBanner1ShowType: TIntegerField
      FieldName = 'ShowType'
    end
    object tblPlayBanner1ShowPosition: TIntegerField
      FieldName = 'ShowPosition'
    end
    object tblPlayBanner1FileSize: TIntegerField
      FieldName = 'FileSize'
    end
    object tblPlayBanner1FileLocation: TWideStringField
      FieldName = 'FileLocation'
      Size = 255
    end
    object tblPlayBanner1FileHash: TWideStringField
      FieldName = 'FileHash'
      Size = 50
    end
    object tblPlayBanner1Ordering: TIntegerField
      FieldName = 'Ordering'
    end
  end
  object tblPlayBanner2: TADOTable
    Connection = db
    CursorType = ctStatic
    IndexFieldNames = 'Ordering'
    TableName = 'QryPlayBanner2'
    Left = 384
    Top = 88
    object tblPlayBanner2QID: TIntegerField
      FieldName = 'QID'
    end
    object tblPlayBanner2id: TIntegerField
      FieldName = 'id'
    end
    object tblPlayBanner2FileName: TWideStringField
      FieldName = 'FileName'
      Size = 255
    end
    object tblPlayBanner2NewFileName: TWideStringField
      FieldName = 'NewFileName'
      Size = 255
    end
    object tblPlayBanner2ShowType: TIntegerField
      FieldName = 'ShowType'
    end
    object tblPlayBanner2ShowPosition: TIntegerField
      FieldName = 'ShowPosition'
    end
    object tblPlayBanner2FileSize: TIntegerField
      FieldName = 'FileSize'
    end
    object tblPlayBanner2FileLocation: TWideStringField
      FieldName = 'FileLocation'
      Size = 255
    end
    object tblPlayBanner2FileHash: TWideStringField
      FieldName = 'FileHash'
      Size = 50
    end
    object tblPlayBanner2Ordering: TIntegerField
      FieldName = 'Ordering'
    end
  end
  object tblPlayVideo: TADOTable
    Connection = db
    CursorType = ctStatic
    IndexFieldNames = 'ordering'
    TableName = 'QryPlayVideo'
    Left = 384
    Top = 144
    object tblPlayVideoID: TIntegerField
      FieldName = 'ID'
    end
    object tblPlayVideoQID: TIntegerField
      FieldName = 'QID'
    end
    object tblPlayVideoClientID: TIntegerField
      FieldName = 'ClientID'
    end
    object tblPlayVideoTerminalID: TIntegerField
      FieldName = 'TerminalID'
    end
    object tblPlayVideoFileName: TWideStringField
      FieldName = 'FileName'
      Size = 255
    end
    object tblPlayVideoNewFileName: TWideStringField
      FieldName = 'NewFileName'
      Size = 255
    end
    object tblPlayVideoFileType: TWideStringField
      FieldName = 'FileType'
      Size = 255
    end
    object tblPlayVideoFileSize: TIntegerField
      FieldName = 'FileSize'
    end
    object tblPlayVideoFileLocation: TWideStringField
      FieldName = 'FileLocation'
      Size = 255
    end
    object tblPlayVideoFileHash: TWideStringField
      FieldName = 'FileHash'
      Size = 50
    end
    object tblPlayVideoActive: TBooleanField
      FieldName = 'Active'
    end
    object tblPlayVideoAvail: TBooleanField
      FieldName = 'Avail'
    end
    object tblPlayVideoOrdering: TIntegerField
      FieldName = 'Ordering'
    end
  end
  object tblPlayNews: TADOTable
    Connection = db
    CursorType = ctStatic
    IndexFieldNames = 'id'
    TableName = 'QryPlayNews'
    Left = 384
    Top = 200
    object tblPlayNewsQID: TIntegerField
      FieldName = 'QID'
    end
    object tblPlayNewsid: TIntegerField
      FieldName = 'id'
    end
    object tblPlayNewsHeadLine: TWideStringField
      FieldName = 'HeadLine'
      Size = 255
    end
  end
  object tblPlayLayout: TADOTable
    Connection = db
    CursorType = ctStatic
    IndexFieldNames = 'id'
    TableName = 'QryPlayLayout'
    Left = 384
    Top = 256
    object tblPlayLayoutID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object tblPlayLayoutClientID: TIntegerField
      FieldName = 'ClientID'
    end
    object tblPlayLayoutTerminalID: TIntegerField
      FieldName = 'TerminalID'
    end
    object tblPlayLayoutQID: TIntegerField
      FieldName = 'QID'
    end
    object tblPlayLayoutshow_news: TBooleanField
      FieldName = 'show_news'
    end
    object tblPlayLayoutshow_ads: TBooleanField
      FieldName = 'show_ads'
    end
    object tblPlayLayoutDefault_video: TWideStringField
      FieldName = 'Default_video'
      Size = 100
    end
    object tblPlayLayoutvideo_directory: TWideStringField
      FieldName = 'video_directory'
      Size = 255
    end
    object tblPlayLayouthttp_server: TWideStringField
      FieldName = 'http_server'
      Size = 100
    end
    object tblPlayLayoutvideo_program: TWideStringField
      FieldName = 'video_program'
      Size = 50
    end
    object tblPlayLayoutnews_program: TWideStringField
      FieldName = 'news_program'
      Size = 50
    end
    object tblPlayLayoutrefresh_time: TIntegerField
      FieldName = 'refresh_time'
    end
    object tblPlayLayoutftp_server: TWideStringField
      FieldName = 'ftp_server'
      Size = 200
    end
    object tblPlayLayoutftp_port: TIntegerField
      FieldName = 'ftp_port'
    end
    object tblPlayLayoutftp_user: TWideStringField
      FieldName = 'ftp_user'
      Size = 50
    end
    object tblPlayLayoutftp_pass: TWideStringField
      FieldName = 'ftp_pass'
      Size = 50
    end
    object tblPlayLayoutftp_path: TWideStringField
      FieldName = 'ftp_path'
      Size = 100
    end
    object tblPlayLayoutftp_passive: TBooleanField
      FieldName = 'ftp_passive'
    end
    object tblPlayLayoutdebug_mode: TBooleanField
      FieldName = 'debug_mode'
    end
    object tblPlayLayoutsys_pwd: TWideStringField
      FieldName = 'sys_pwd'
      Size = 255
    end
    object tblPlayLayoutnews_font: TWideStringField
      FieldName = 'news_font'
      Size = 255
    end
    object tblPlayLayoutnews_size: TIntegerField
      FieldName = 'news_size'
    end
    object tblPlayLayoutnews_color: TIntegerField
      FieldName = 'news_color'
    end
    object tblPlayLayoutnews_isbold: TIntegerField
      FieldName = 'news_isbold'
    end
    object tblPlayLayoutnews_isitalic: TIntegerField
      FieldName = 'news_isitalic'
    end
    object tblPlayLayoutnews_isunderline: TIntegerField
      FieldName = 'news_isunderline'
    end
    object tblPlayLayoutbanner_mode: TIntegerField
      FieldName = 'banner_mode'
    end
    object tblPlayLayoutbanner_show_interval: TIntegerField
      FieldName = 'banner_show_interval'
    end
    object tblPlayLayoutnews_delay: TIntegerField
      FieldName = 'news_delay'
    end
    object tblPlayLayoutnews_height: TIntegerField
      FieldName = 'news_height'
    end
    object tblPlayLayoutbanner_width: TIntegerField
      FieldName = 'banner_width'
    end
    object tblPlayLayoutnews_back_color: TIntegerField
      FieldName = 'news_back_color'
    end
    object tblPlayLayoutnews_step: TIntegerField
      FieldName = 'news_step'
    end
    object tblPlayLayoutbanner_prop: TBooleanField
      FieldName = 'banner_prop'
    end
    object tblPlayLayoutshow_clock: TBooleanField
      FieldName = 'show_clock'
    end
    object tblPlayLayoutrss_active: TBooleanField
      FieldName = 'rss_active'
    end
    object tblPlayLayoutrss_url: TWideStringField
      FieldName = 'rss_url'
      Size = 255
    end
    object tblPlayLayoutvideo_source: TBooleanField
      FieldName = 'video_source'
    end
    object tblPlayLayoutlogo_width: TIntegerField
      FieldName = 'logo_width'
    end
    object tblPlayLayoutlogo_stretch: TBooleanField
      FieldName = 'logo_stretch'
    end
    object tblPlayLayoutlogo_ratio: TBooleanField
      FieldName = 'logo_ratio'
    end
    object tblPlayLayoutdef_type: TIntegerField
      FieldName = 'def_type'
    end
    object tblPlayLayoutdef_file: TWideStringField
      FieldName = 'def_file'
      Size = 128
    end
  end
  object tblQueuePlay: TADOTable
    Connection = db
    CursorType = ctStatic
    IndexFieldNames = 'id'
    TableName = 'QryQueuePlay'
    Left = 384
    Top = 304
    object tblQueuePlayid: TIntegerField
      FieldName = 'id'
    end
    object tblQueuePlayQName: TWideStringField
      FieldName = 'QName'
      Size = 255
    end
    object tblQueuePlayStartDate: TDateTimeField
      FieldName = 'StartDate'
    end
    object tblQueuePlayStopDate: TDateTimeField
      FieldName = 'StopDate'
    end
    object tblQueuePlaySTime: TDateTimeField
      FieldName = 'STime'
      ReadOnly = True
    end
    object tblQueuePlayFTime: TDateTimeField
      FieldName = 'FTime'
      ReadOnly = True
    end
    object tblQueuePlayCTime: TDateTimeField
      FieldName = 'CTime'
      ReadOnly = True
    end
  end
  object tblLogPlay: TADOTable
    Connection = db
    TableName = 'log_play'
    Left = 384
    Top = 368
    object tblLogPlayID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object tblLogPlayqid: TIntegerField
      FieldName = 'qid'
    end
    object tblLogPlaylog_id: TIntegerField
      FieldName = 'log_id'
    end
    object tblLogPlaytype: TWideStringField
      FieldName = 'type'
      Size = 50
    end
    object tblLogPlayfile: TWideStringField
      FieldName = 'file'
      Size = 50
    end
    object tblLogPlayaction: TWideStringField
      FieldName = 'action'
      Size = 50
    end
    object tblLogPlaycdate: TWideStringField
      FieldName = 'cdate'
      Size = 50
    end
  end
end
