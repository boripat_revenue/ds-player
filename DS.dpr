program ds;

uses
  Forms,
  fmVideo in 'fmVideo.pas' {frmVideo},
  data in 'data.pas' {dm: TDataModule},
  fmPlacement in 'fmPlacement.pas' {frmPlacement},
  ceffilescheme in 'ceffilescheme.pas',
  ceflib in 'ceflib.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'DS Player';
  Application.CreateForm(TfrmVideo, frmVideo);
  Application.CreateForm(Tdm, dm);
  Application.Run;
end.
